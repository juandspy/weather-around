package main

import (
	"net/http"
	"os"

	log "github.com/sirupsen/logrus"
)

const servingAddress = ":5000"

func main() {
	s := Server{
		geoNamesGetter: GetGeoNames,
		weatherGetter:  GetWeather,
	}
	var port = os.Getenv("PORT")
	if port == "" {
		port = "5000"
		log.Info("INFO: No PORT environment variable detected, defaulting to " + port)
	}

	log.Infof("Serving at %q", servingAddress)
	log.Fatal(http.ListenAndServe(":"+port, &s))
}
