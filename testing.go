package main

import "errors"

const (
	rad int     = 50 // km
	lat float64 = 43.3619145
	lng float64 = -5.8493887
)

var StubGeonames = GeoNamesAPIResponse{
	Geonames: []GeoName{
		GeoName{
			Name:      "Oviedo",
			Longitude: "-5.84476",
			Latitude:  "43.36029",
		},
		GeoName{
			Name:      "Lugones",
			Longitude: "-5.81215",
			Latitude:  "43.40272",
		},
	},
}

// StubWeather is a stub object of type WttrAPIResponse with 3 days and 2 hourly fields.
var StubWeather = WttrAPIResponse{
	CurrentCondition: []CurrentConditionField{
		CurrentConditionField{
			Cloudcover:    "75",
			Humidity:      "83",
			Precipmm:      "0.0",
			TempC:         "17",
			Windspeedkmph: "9",
			Weatherdesc: []WeatherdescField{
				WeatherdescField{
					Value: "Partly cloudy",
				},
			},
		},
	},

	Weather: []WeatherField{
		WeatherField{
			Avgtempc: "17",
			Maxtempc: "24",
			Mintempc: "9",
			Date:     "2021-07-15",
			Hourly: []HourlyField{
				HourlyField{
					Dewpointc:    "8",
					Feelslikec:   "9",
					Heatindexc:   "9",
					Windchillc:   "9",
					Windgustkmph: "7",
					Chanceofrain: "0",
					Cloudcover:   "26",
					Precipmm:     "0.0",
					Tempc:        "9",
					Time:         "0",
					Weatherdesc: []WeatherdescField{
						WeatherdescField{
							Value: "Mist",
						},
					},
					Windspeedkmph: "3",
				},
				HourlyField{
					Dewpointc:    "9",
					Feelslikec:   "10",
					Heatindexc:   "10",
					Windchillc:   "10",
					Windgustkmph: "11",
					Chanceofrain: "0",
					Cloudcover:   "14",
					Precipmm:     "0.0",
					Tempc:        "10",
					Time:         "300",
					Weatherdesc: []WeatherdescField{
						WeatherdescField{
							Value: "Mist",
						},
					},
					Windspeedkmph: "6",
				},
				// The rest of the hours for this day ...
			},
		},
		WeatherField{
			Avgtempc: "18",
			Maxtempc: "26",
			Mintempc: "13",
			Date:     "2021-07-16",
			Hourly: []HourlyField{
				HourlyField{
					Dewpointc:    "13",
					Feelslikec:   "15",
					Heatindexc:   "16",
					Windchillc:   "15",
					Windgustkmph: "8",
					Chanceofrain: "0",
					Cloudcover:   "9",
					Precipmm:     "0.0",
					Tempc:        "14",
					Time:         "0",
					Weatherdesc: []WeatherdescField{
						WeatherdescField{
							Value: "Mist",
						},
					},
					Windspeedkmph: "5",
				},
				HourlyField{
					Dewpointc:    "12",
					Feelslikec:   "14",
					Heatindexc:   "13",
					Windchillc:   "14",
					Windgustkmph: "9",
					Chanceofrain: "0",
					Cloudcover:   "3",
					Precipmm:     "0.0",
					Tempc:        "13",
					Time:         "300",
					Weatherdesc: []WeatherdescField{
						WeatherdescField{
							Value: "Mist",
						},
					},
					Windspeedkmph: "5",
				},
				// The rest of the hours for this day ...
			},
		},

		WeatherField{
			Avgtempc: "21",
			Maxtempc: "28",
			Mintempc: "16",
			Date:     "2021-07-17",
			Hourly: []HourlyField{
				HourlyField{
					Dewpointc:    "14",
					Feelslikec:   "16",
					Heatindexc:   "18",
					Windchillc:   "16",
					Windgustkmph: "9",
					Chanceofrain: "0",
					Cloudcover:   "1",
					Precipmm:     "0.0",
					Tempc:        "16",
					Time:         "0",
					Weatherdesc: []WeatherdescField{
						WeatherdescField{
							Value: "Mist",
						},
					},
					Windspeedkmph: "5",
				},
				HourlyField{
					Dewpointc:    "15",
					Feelslikec:   "16",
					Heatindexc:   "16",
					Windchillc:   "16",
					Windgustkmph: "9",
					Chanceofrain: "0",
					Cloudcover:   "0",
					Precipmm:     "0.0",
					Tempc:        "16",
					Time:         "300",
					Weatherdesc: []WeatherdescField{
						WeatherdescField{
							Value: "Clear",
						},
					},
					Windspeedkmph: "5",
				},
				// The rest of the hours for this day ...
			},
		},
	},
}

func StubGeoNamesGetter(longitude float64, latitude float64, radius int) (GeoNamesAPIResponse, error) {
	return StubGeonames, nil
}

func StubWeatherGetter(latitude float64, longitude float64) (WttrAPIResponse, error) {
	return StubWeather, nil
}

func StubWeatherGetterWithError(latitude float64, longitude float64) (WttrAPIResponse, error) {
	return WttrAPIResponse{}, errors.New("random error")
}
