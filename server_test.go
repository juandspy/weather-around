package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestPOSTData(t *testing.T) {
	var jsonStr = []byte(`{"random": "message"}`)
	server := Server{}

	request, err := http.NewRequest(http.MethodPost, "/weather-around", bytes.NewBuffer(jsonStr))
	if err != nil {
		t.Errorf("got error while making the request, %v", err)
	}
	response := httptest.NewRecorder()

	server.ServeHTTP(response, request)

	assertStatus(t, response.Code, http.StatusMethodNotAllowed)
}

func TestGetData(t *testing.T) {
	server := Server{
		geoNamesGetter: StubGeoNamesGetter,
		weatherGetter:  StubWeatherGetter,
	}

	request, err := http.NewRequest(http.MethodGet, fmt.Sprintf("/weather-around?latitude=%f&longitude=%f&radius=%d", lat, lng, rad), nil)
	if err != nil {
		t.Errorf("got error while making the request, %v", err)
	}
	response := httptest.NewRecorder()

	server.ServeHTTP(response, request)

	assertStatus(t, response.Code, http.StatusOK)

	var result []map[string]interface{}
	err = json.NewDecoder(response.Body).Decode(&result)
	if err != nil {
		t.Errorf("got error while decoding the response, %v", err)
	}

	for _, m := range result {
		if m["name"] != "Oviedo" && m["name"] != "Lugones" {
			t.Error("expected 'Oviedo' and 'Lugones' inside response body")
		}
	}

}

func TestGetDataWithErrorInGorutine(t *testing.T) {
	server := Server{
		geoNamesGetter: StubGeoNamesGetter,
		weatherGetter:  StubWeatherGetterWithError,
	}

	request, err := http.NewRequest(http.MethodGet, fmt.Sprintf("/weather-around?latitude=%f&longitude=%f&radius=%d", lat, lng, rad), nil)
	if err != nil {
		t.Errorf("got error while making the request, %v", err)
	}
	response := httptest.NewRecorder()

	server.ServeHTTP(response, request)

	assertStatus(t, response.Code, http.StatusInternalServerError)

	fmt.Println(response.Body)

}

func TestGetDataNotEnoughArguments(t *testing.T) {
	server := Server{}

	request, err := http.NewRequest(http.MethodGet, fmt.Sprintf("/weather-around?longitude=%f&radius=%d", lng, rad), nil)
	if err != nil {
		t.Errorf("got error while making the request, %v", err)
	}
	response := httptest.NewRecorder()

	server.ServeHTTP(response, request)

	assertStatus(t, response.Code, http.StatusBadRequest)
}

func assertStatus(t *testing.T, got, want int) {
	if got != want {
		t.Errorf("got status %v, want %v", got, want)
	}
}
