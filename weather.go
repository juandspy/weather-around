package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
)

type CurrentConditionField struct {
	Cloudcover    string             `json:"cloudcover"`
	Humidity      string             `json:"humidity"`
	Precipmm      string             `json:"precipMM"`
	TempC         string             `json:"temp_C"`
	Weatherdesc   []WeatherdescField `json:"weatherDesc"`
	Windspeedkmph string             `json:"windspeedKmph"`
}

type WeatherdescField struct {
	Value string `json:"value"`
}

type WeatherField struct {
	Avgtempc string        `json:"avgtempC"`
	Maxtempc string        `json:"maxtempC"`
	Mintempc string        `json:"mintempC"`
	Date     string        `json:"date"`
	Hourly   []HourlyField `json:"hourly"`
}

type HourlyField struct {
	Dewpointc     string             `json:"DewPointC"`
	Feelslikec    string             `json:"FeelsLikeC"`
	Heatindexc    string             `json:"HeatIndexC"`
	Windchillc    string             `json:"WindChillC"`
	Windgustkmph  string             `json:"WindGustKmph"`
	Chanceofrain  string             `json:"chanceofrain"`
	Cloudcover    string             `json:"cloudcover"`
	Precipmm      string             `json:"precipMM"`
	Tempc         string             `json:"tempC"`
	Time          string             `json:"time"`
	Weatherdesc   []WeatherdescField `json:"weatherDesc"`
	Windspeedkmph string             `json:"windspeedKmph"`
}

type NearestAreaField struct {
	Latitude  string `json:"latitude"`
	Longitude string `json:"longitude"`
}

type WttrAPIResponse struct {
	CurrentCondition []CurrentConditionField `json:"current_condition"`
	NearestArea      []NearestAreaField      `json:"nearest_area"`
	Weather          []WeatherField          `json:"weather"`
}

func queryWeather(latitude float64, longitude float64) ([]byte, error) {
	resp, err := http.Get(fmt.Sprintf("http://wttr.in/%f,%f?m&format=j1", latitude, longitude))
	if err != nil {
		return nil, err
	}

	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	return body, nil
}

func parseWeather(body []byte) (WttrAPIResponse, error) {
	var resp = new(WttrAPIResponse)
	err := json.Unmarshal(body, &resp)
	if err != nil {
		return WttrAPIResponse{}, err
	}
	return WttrAPIResponse{
		CurrentCondition: resp.CurrentCondition,
		NearestArea:      resp.NearestArea,
		Weather:          resp.Weather,
	}, nil
}

func GetWeather(latitude float64, longitude float64) (WttrAPIResponse, error) {
	body, err := queryWeather(latitude, longitude)
	if err != nil {
		return WttrAPIResponse{}, err
	}

	parsedWeather, err := parseWeather(body)
	if err != nil {
		return WttrAPIResponse{}, err
	}
	return parsedWeather, nil
}
