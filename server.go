package main

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"

	log "github.com/sirupsen/logrus"
)

type GeoNamesGetter func(longitude float64, latitude float64, radius int) (GeoNamesAPIResponse, error)
type WeatherGetter func(latitude float64, longitude float64) (WttrAPIResponse, error)

type Server struct {
	geoNamesGetter GeoNamesGetter
	weatherGetter  WeatherGetter
}

func (s *Server) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	router := http.NewServeMux()
	router.Handle("/weather-around", http.HandlerFunc(s.handleRequest))
	router.ServeHTTP(w, r)
}

type httpErrorStruct struct {
	errorMsg   string
	statusCode int
}

func (s *Server) getGeoNameAndWeather(latitude float64, longitude float64, radius int) (serverResponse []JoinedWeatherResponse, errorMsg string, statusCode int) {
	log.Info("getGeoNameAndWeather uses ", latitude, longitude, radius)
	geonamesAPIResponse, err := s.geoNamesGetter(latitude, longitude, radius)
	if err != nil {
		return serverResponse, "error retrieving closest cities", http.StatusInternalServerError
	}

	joinedWeatherChannel := make(chan JoinedWeatherResponse)
	errorChannel := make(chan httpErrorStruct)

	for _, geoname := range geonamesAPIResponse.Geonames {
		go func(g GeoName) {
			gLat, err := strconv.ParseFloat(g.Latitude, 64)
			if err != nil {
				errorChannel <- httpErrorStruct{
					errorMsg:   fmt.Sprintf("error casting Geoname latitude %s to float", g.Latitude),
					statusCode: http.StatusInternalServerError,
				}
				return
			}
			gLon, err := strconv.ParseFloat(g.Longitude, 64)
			if err != nil {
				errorChannel <- httpErrorStruct{
					errorMsg:   fmt.Sprintf("error casting Geoname longitude %s to float", g.Longitude),
					statusCode: http.StatusInternalServerError,
				}
				return
			}
			weather, err := s.weatherGetter(gLat, gLon)
			if err != nil {
				errorChannel <- httpErrorStruct{
					errorMsg:   fmt.Sprintf("error querying the weather for location %f, %f: %v", gLat, gLon, err),
					statusCode: http.StatusInternalServerError,
				}
				return
			}
			joinedWeather, err := joinGeoNamesWeather(g, weather)
			if err != nil {
				errorChannel <- httpErrorStruct{
					errorMsg:   fmt.Sprintf("error joining geoname and weather : %v", err),
					statusCode: http.StatusInternalServerError,
				}
				return
			}
			joinedWeatherChannel <- joinedWeather
		}(geoname)
	}
	validAnswersCount := 0
	for {
		select {
		case err := <-errorChannel:
			log.WithFields(log.Fields{"error_message": err.errorMsg}).Info("Error parsing attributes")
			return []JoinedWeatherResponse{}, "error parsing attributes", http.StatusInternalServerError
		case j := <-joinedWeatherChannel:
			validAnswersCount++
			serverResponse = append(serverResponse, j)
			if validAnswersCount == len(geonamesAPIResponse.Geonames) {
				return serverResponse, "", http.StatusOK
			}
		}
	}
}

func (s *Server) handleRequest(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "text/html; charset=utf-8")
	w.Header().Set("Access-Control-Allow-Origin", "*")

	if r.Method != http.MethodGet {
		http.Error(w, "Only GET method allowed", http.StatusMethodNotAllowed)
		return
	}
	latitude, longitude, radius, errorMsg, statusCode := parseFields(r)
	if errorMsg != "" {
		http.Error(w, errorMsg, statusCode)
		return
	}

	log.WithFields(log.Fields{
		"latitude":  latitude,
		"longitude": longitude,
		"radius":    radius,
	}).Info("Got a valid request")

	serverResponse, errorMsg, statusCode := s.getGeoNameAndWeather(latitude, longitude, radius)
	if errorMsg != "" {
		http.Error(w, errorMsg, statusCode)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(serverResponse)
}

func parseFields(r *http.Request) (latitude float64, longitude float64, radius int, errorMsg string, statusCode int) {
	query := r.URL.Query()
	strLatitude := query.Get("latitude")
	strLongitude := query.Get("longitude")
	strRadius := query.Get("radius")

	if strLatitude == "" || strLongitude == "" || strRadius == "" {
		return 0, 0, 0, "not enough arguments, expecting latitude, longitude and radius", http.StatusBadRequest
	}

	latitude, err := strconv.ParseFloat(strLatitude, 64)
	if err != nil {
		return 0, 0, 0, "invalid type of latitude, expecting a float number", http.StatusBadRequest
	}
	longitude, err = strconv.ParseFloat(strLongitude, 64)
	if err != nil {
		return 0, 0, 0, "invalid type of longitude, expecting a float number", http.StatusBadRequest
	}
	radius, err = strconv.Atoi(strRadius)
	if err != nil {
		return 0, 0, 0, "invalid type of radius, expecting an int", http.StatusBadRequest
	}
	if radius > 300 {
		return 0, 0, 0, "radius must be lower than 300km", http.StatusBadRequest
	}

	return latitude, longitude, radius, "", http.StatusOK
}
