package main

import (
	"fmt"
	"reflect"
	"testing"
)

func TestJoiner(t *testing.T) {
	var stubGeoname = StubGeonames.Geonames[0]
	joinedWeather, err := joinGeoNamesWeather(stubGeoname, StubWeather)
	if err != nil {
		t.Error(err)
	}
	if reflect.DeepEqual(joinedWeather, JoinedWeatherResponse{}) {
		t.Error("expected to have a joinedWeather not nil")
	} else {
		if joinedWeather.Name == "" {
			t.Error("expected joinedWeather Name not empty")
		}
		if joinedWeather.Latitude == "" {
			t.Error("expected joinedWeather Latitude not empty")
		}
		if joinedWeather.Longitude == "" {
			t.Error("expected joinedWeather Longitude not empty")
		}
		if len(joinedWeather.Weather.Weather) < 3 {
			t.Error("expected joinedWeather to have at least three days of data ")
		} else {
			if len(joinedWeather.Weather.Weather[0].Hourly) < 2 {
				t.Error("expected joinedWeather daily weather to have at least 2 hourly data")
			}

			if joinedWeather.Weather.Weather[0].AvgCloudCover == "" {
				t.Error("expected AvgCloudCover not empty")
			}
			if joinedWeather.Weather.Weather[0].MaxChanceOfRain == "" {
				t.Error("expected MaxChanceOfRain not empty")
			}
			if joinedWeather.Weather.Weather[0].MmAccumulated == "" {
				t.Error("expected MmAccumulated not empty")
			}
		}
	}
}

func TestWeatherAccumulator(t *testing.T) {
	var stubWeatherField = WeatherField{
		Hourly: []HourlyField{
			HourlyField{
				Chanceofrain: "10",
				Cloudcover:   "5",
				Precipmm:     "3.0",
			},
			HourlyField{
				Chanceofrain: "20",
				Cloudcover:   "10",
				Precipmm:     "0.0",
			},
			// The rest of the hours for this day ...
		},
	}

	want := ExtendedWeatherField{
		AvgCloudCover:   "7.5",
		MaxChanceOfRain: "20",
		MmAccumulated:   "3.0",
		// The rest of fields ...
	}

	extendedWeatherField, err := weatherAccumulator(stubWeatherField)
	fmt.Println(extendedWeatherField, err)

	if err != nil {
		t.Error(err)
	}

	if want.AvgCloudCover != extendedWeatherField.AvgCloudCover {
		t.Errorf("got AvgCloudCover %s want %s", extendedWeatherField.AvgCloudCover, want.AvgCloudCover)
	}

	if want.MaxChanceOfRain != extendedWeatherField.MaxChanceOfRain {
		t.Errorf("got MaxChanceOfRain %s want %s", extendedWeatherField.MaxChanceOfRain, want.MaxChanceOfRain)
	}

	if want.MmAccumulated != extendedWeatherField.MmAccumulated {
		t.Errorf("got MmAccumulated %s want %s", extendedWeatherField.MmAccumulated, want.MmAccumulated)
	}

}
