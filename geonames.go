package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"

	log "github.com/sirupsen/logrus"
)

const (
	maxRows       int    = 10
	minPopulation int    = 5000 // TODO: Assert value in 1000, 5000 or 15000
	language      string = "es" // language of GeoNames response
)

var username = os.Getenv("GeoNames_username")

type GeoName struct {
	Name      string `json:"name"`
	Longitude string `json:"lng"`
	Latitude  string `json:"lat"`
}

type GeoNamesAPIResponse struct {
	Geonames []GeoName `json:"geonames"`
}

func queryGeoNames(latitude float64, longitude float64, radius int) ([]byte, error) {
	if radius > 300 {
		return nil, errors.New(fmt.Sprint("radius must be less than 300, got ", radius))
	}

	if username == "" {
		log.Panic("GeoNames_username environment variable not set, cannot use API")
		return nil, errors.New("GeoNames_username environment variable not set, cannot use API")
	}
	client := &http.Client{}

	req, err := http.NewRequest("GET", "http://api.geonames.org/findNearbyPlaceNameJSON", nil)
	if err != nil {
		return nil, err
	}

	q := req.URL.Query()
	q.Add("radius", fmt.Sprint(radius))
	q.Add("maxRows", fmt.Sprint(maxRows))
	q.Add("lat", fmt.Sprint(latitude))
	q.Add("lng", fmt.Sprint(longitude))
	q.Add("lang", language)
	q.Add("cities", fmt.Sprint("cities", minPopulation))
	q.Add("username", username)
	req.URL.RawQuery = q.Encode()

	log.Infof("getting URL %s", req.URL)
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}

	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	return body, nil
}

func parseGeoNames(body []byte) (GeoNamesAPIResponse, error) {
	var resp = new(GeoNamesAPIResponse)
	err := json.Unmarshal(body, &resp)
	if err != nil {
		return GeoNamesAPIResponse{}, err
	}
	return GeoNamesAPIResponse{
		Geonames: resp.Geonames,
	}, nil
}

func GetGeoNames(longitude float64, latitude float64, radius int) (GeoNamesAPIResponse, error) {
	body, err := queryGeoNames(longitude, latitude, radius)
	if err != nil {
		return GeoNamesAPIResponse{}, err
	}
	parsedGeonames, err := parseGeoNames(body)
	if err != nil {
		return GeoNamesAPIResponse{}, err
	}

	return parsedGeonames, nil
}
