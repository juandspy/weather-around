package main

import (
	"testing"
)

func TestListCities(t *testing.T) {
	parsedGeonames, err := GetGeoNames(lat, lng, rad)
	if err != nil {
		t.Error(err)
	}

	// Check that there are at least two cities in the response
	got := len(parsedGeonames.Geonames)
	want := 2
	if got < want {
		t.Errorf("expected a response length of at least %d, got %d", want, got)
	}
}

func TestMaxRadius(t *testing.T) {
	_, err := queryGeoNames(lng, lat, 301)
	if err == nil {
		t.Error("expected to receive an error")
	}

	_, err = queryGeoNames(lng, lat, 300)
	if err != nil {
		t.Errorf("didn't expect to receive an error, got %q", err)
	}
}
