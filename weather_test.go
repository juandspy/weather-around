package main

import (
	"reflect"
	"testing"
)

func TestQueryWeather(t *testing.T) {
	_, err := queryWeather(lat, lng)
	if err != nil {
		t.Error(err)
	}
}

func TestGetWeather(t *testing.T) {
	parsedWeather, err := GetWeather(lat, lng)
	if err != nil {
		t.Error(err)
	}

	if reflect.DeepEqual(parsedWeather, &WttrAPIResponse{}) {
		t.Error("expected parsedWeather not empty")
	}

	// TODO: Check all fields are filled
}
