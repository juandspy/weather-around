package main

import (
	"fmt"
	"sort"
	"strconv"
)

type JoinedWeatherResponse struct {
	Name      string                  `json:"name"`
	Longitude string                  `json:"lng"`
	Latitude  string                  `json:"lat"`
	Weather   ExtendedWttrAPIResponse `json:"weather"`
}

type ExtendedWttrAPIResponse struct {
	CurrentCondition []CurrentConditionField `json:"current_condition"`
	NearestArea      []NearestAreaField      `json:"nearest_area"`
	Weather          []ExtendedWeatherField  `json:"weather"`
}

type ExtendedWeatherField struct {
	Avgtempc        string        `json:"avgtempC"`
	Maxtempc        string        `json:"maxtempC"`
	Mintempc        string        `json:"mintempC"`
	AvgCloudCover   string        `json:"avgcloudcover"`
	MaxChanceOfRain string        `json:"maxchanceofrain"`
	MmAccumulated   string        `json:"mmaccumulated"`
	Date            string        `json:"date"`
	Hourly          []HourlyField `json:"hourly"`
}

func joinGeoNamesWeather(geoName GeoName, parsedWeather WttrAPIResponse) (JoinedWeatherResponse, error) {
	// TODO: Check that longitude and latitude from GeoName and Weather nearest area is close enough to avoid misleading addresses.

	var sliceExtendedWeatherField []ExtendedWeatherField
	for _, weather := range parsedWeather.Weather {
		extendedWeather, err := weatherAccumulator(weather)
		if err != nil {
			return JoinedWeatherResponse{}, err
		}
		sliceExtendedWeatherField = append(sliceExtendedWeatherField, extendedWeather)
	}

	var joinedWeatherResponse = JoinedWeatherResponse{
		Name:      geoName.Name,
		Longitude: geoName.Longitude,
		Latitude:  geoName.Latitude,
		Weather: ExtendedWttrAPIResponse{
			CurrentCondition: parsedWeather.CurrentCondition,
			NearestArea:      parsedWeather.NearestArea,
			Weather:          sliceExtendedWeatherField,
		},
	}

	return joinedWeatherResponse, nil
}

func weatherAccumulator(weather WeatherField) (ExtendedWeatherField, error) {
	var sliceCloudCover, sliceChanceOfRain []int
	var sliceMm []float64
	var extendedWeather ExtendedWeatherField

	for _, hourlyWeather := range weather.Hourly {
		intCloudCover, err := strconv.Atoi(hourlyWeather.Cloudcover)
		if err != nil {
			return extendedWeather, fmt.Errorf("error parsing cloudcover %s to int", hourlyWeather.Cloudcover)
		}
		intChanceOfRain, err := strconv.Atoi(hourlyWeather.Chanceofrain)
		if err != nil {
			return extendedWeather, fmt.Errorf("error parsing chandeofrain %s to int", hourlyWeather.Chanceofrain)
		}
		floatPrecipmm, err := strconv.ParseFloat(hourlyWeather.Precipmm, 64)
		if err != nil {
			return extendedWeather, fmt.Errorf("error parsing precipmm %s to float64", hourlyWeather.Precipmm)
		}

		sliceCloudCover = append(sliceCloudCover, intCloudCover)
		sliceChanceOfRain = append(sliceChanceOfRain, intChanceOfRain)
		sliceMm = append(sliceMm, floatPrecipmm)
	}

	return ExtendedWeatherField{
		Avgtempc:        weather.Avgtempc,
		Maxtempc:        weather.Maxtempc,
		Mintempc:        weather.Mintempc,
		AvgCloudCover:   fmt.Sprint(sliceAvg(sliceCloudCover)),
		MaxChanceOfRain: fmt.Sprint(sliceMax(sliceChanceOfRain)),
		MmAccumulated:   fmt.Sprintf("%.1f", sliceSum(sliceMm)),
		Date:            weather.Date,
		Hourly:          weather.Hourly,
	}, nil
}

func sliceAvg(inputSlice []int) float64 {
	sum := 0
	for _, value := range inputSlice {
		sum = sum + value
	}
	return float64(sum) / float64(len(inputSlice))
}

func sliceSum(inputSlice []float64) (sum float64) {
	for _, value := range inputSlice {
		sum = sum + value
	}
	return sum
}

func sliceMax(inputSlice []int) int {
	sort.Ints(inputSlice)
	return inputSlice[len(inputSlice)-1]
}
